# KnownCMS

#### 介绍

`KnownCMS`是基于`Blazor`开发的一个内容管理网站，网站前台使用`Blazor`静态组件，后台使用`Known`框架。

![输入图片说明](https://foruda.gitee.com/images/1727322736359863475/b49a0e20_14334.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1727322783597844539/eac6cf86_14334.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1727322688669266612/0b76c1e1_14334.png "屏幕截图")

#### 软件架构

- 前台采用静态`Blazor`组件，采用`Http`协议与后端交互
- 后台使用`Known`开发框架进行开发，采用交互式`SSR`模式

```
├─AntBlazor     -> AntDesign静态组件库。
├─KnownCMS      -> 系统前台页面组件库。
├─KnownCMS.Web  -> 系统后台管理。
├─WebSite       -> 前台网站。
├─KnownCMS.sln  -> 解决方案文件。
```

#### 使用说明

- 数据库默认根目录`SQLite`库`CMSLite.db`
- 前台用户名：`known`，密码：`1`
- 后台用户名：`Admin`，密码：`1`

#### 赞助

**感谢`Ant Design Blazor`的大力赞助。**

> 如果你觉得这个项目对你有帮助，你可以请作者喝杯咖啡表示鼓励 ☕️

![输入图片说明](https://foruda.gitee.com/images/1726452783813098766/71768ec0_14334.png "屏幕截图")
