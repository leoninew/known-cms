﻿namespace KnownCMS.Services;

/// <summary>
/// 内容站点服务接口。
/// </summary>
public interface ISiteService : IService
{
    //Category
    /// <summary>
    /// 异步获取内容类别列表。
    /// </summary>
    /// <param name="type">内容类型。</param>
    /// <returns>内容类别列表。</returns>
    Task<List<CmCategory>> GetCategoriesAsync(ContentType type);

    //Interact
    /// <summary>
    /// 异步分页查询内容列表。
    /// </summary>
    /// <param name="criteria">查询条件。</param>
    /// <returns>分页结果。</returns>
    Task<PagingResult<PostListInfo>> QueryPostsAsync(PagingCriteria criteria);

    /// <summary>
    /// 异步获取内容排名列表。
    /// </summary>
    /// <param name="type">内容类型。</param>
    /// <returns>内容列表。</returns>
    Task<List<PostListInfo>> GetRankPostsAsync(ContentType type);

    /// <summary>
    /// 异步获取内容明细信息。
    /// </summary>
    /// <param name="type">内容类型。</param>
    /// <param name="code">内容代码或ID。</param>
    /// <returns>内容明细信息。</returns>
    Task<PostDetailInfo> GetPostAsync(ContentType type, string code);

    /// <summary>
    /// 异步保存发布的内容信息。
    /// </summary>
    /// <param name="info">内容表单信息。</param>
    /// <returns>保存结果。</returns>
    Task<Result> SavePostAsync(PostFormInfo info);

    /// <summary>
    /// 异步分页查询回复列表。
    /// </summary>
    /// <param name="criteria">查询条件。</param>
    /// <returns>分页结果。</returns>
    Task<PagingResult<ReplyListInfo>> QueryRepliesAsync(PagingCriteria criteria);

    /// <summary>
    /// 异步保存回复内容信息。
    /// </summary>
    /// <param name="info">回复表单信息。</param>
    /// <returns>保存结果。</returns>
    Task<Result> SaveReplyAsync(ReplyFormInfo info);

    /// <summary>
    /// 异步获取系统用户信息。
    /// </summary>
    /// <param name="id">用户ID。</param>
    /// <returns>系统用户信息。</returns>
    Task<CmUser> GetUserAsync(string id);

    /// <summary>
    /// 异步保存用户信息。
    /// </summary>
    /// <param name="info">用户表单信息。</param>
    /// <returns>保存结果。</returns>
    Task<Result> SaveUserAsync(UserFormInfo info);

    /// <summary>
    /// 异步保存用户密码。
    /// </summary>
    /// <param name="info">用户密码信息。</param>
    /// <returns>保存结果。</returns>
    Task<Result> SaveUserPwdAsync(PwdFormInfo info);
}

class SiteService(Context context) : ServiceBase(context), ISiteService
{
    //Category
    public Task<List<CmCategory>> GetCategoriesAsync(ContentType type)
    {
        return Database.QueryListAsync<CmCategory>(d => d.Type == type.ToString());
    }

    //Interact
    public Task<PagingResult<PostListInfo>> QueryPostsAsync(PagingCriteria criteria)
    {
        return SiteRepository.QueryPostsAsync(Database, criteria);
    }

    public Task<List<PostListInfo>> GetRankPostsAsync(ContentType type)
    {
        return SiteRepository.GetRankPostsAsync(Database, type);
    }

    public async Task<PostDetailInfo> GetPostAsync(ContentType type, string code)
    {
        var db = Database;
        await db.OpenAsync();
        var post = type == ContentType.Interact
                 ? await db.QueryByIdAsync<CmPost>(code)
                 : await GetPostByDocumentAsync(db, type, code);
        if (post != null)
        {
            //添加查看记录，同IP记录一次
            var ip = Context.IPAddress;
            var userName = Context.CurrentUser?.UserName ?? "Anonymous";
            await db.AddViewLogAsync(post, ip, userName);
        }

        var info = new PostDetailInfo
        {
            Id = post?.Id,
            Title = post?.Title,
            Content = post?.Content,
            Author = post?.CreateBy,
            Tags = post?.Tags,
            ViewQty = post?.ViewQty,
            LikeQty = post?.LikeQty,
            ReplyQty = post?.ReplyQty
        };
        if (post != null)
        {
            if (!string.IsNullOrWhiteSpace(post.UserId))
            {
                var user = await db.QueryByIdAsync<CmUser>(post.UserId);
                info.Author = user?.NickName;
            }
            if (type == ContentType.Interact)
                info.Replies = await GetReplyListsAsync(db, post.Id);
        }
        await db.CloseAsync();
        return info;
    }

    public async Task<Result> SavePostAsync(PostFormInfo info)
    {
        var categories = await Database.QueryListAsync<CmCategory>(d => d.Type == ContentType.Interact.ToString());
        var model = new CmPost
        {
            Type = ContentType.Interact.ToString(),
            Title = info.Title,
            Content = info.Content,
            UserId = CurrentUser?.Id,
            CategoryId = AIHelper.GetPostCategory(categories, info),
            Tags = AIHelper.GetPostTags(info),
            Status = PostStatus.Published
        };
        var vr = model.Validate(Context);
        if (!vr.IsValid)
            return vr;

        return await Database.TransactionAsync(Language.Save, async db =>
        {
            model.PublishTime = DateTime.Now;
            await db.SaveAsync(model);
            await db.AddUserCountAsync(BizType.Content, CurrentUser.Id);
        }, model);
    }

    public Task<PagingResult<ReplyListInfo>> QueryRepliesAsync(PagingCriteria criteria)
    {
        return SiteRepository.QueryRepliesAsync(Database, criteria);
    }

    public async Task<Result> SaveReplyAsync(ReplyFormInfo info)
    {
        var post = await Database.QueryByIdAsync<CmPost>(info.BizId);
        if (post == null)
            return Result.Error("问题不存在！");

        var model = new CmReply
        {
            BizType = post.Type,
            BizId = info.BizId,
            UserId = CurrentUser?.Id,
            Content = info.Content
        };
        var vr = model.Validate(Context);
        if (!vr.IsValid)
            return vr;

        return await Database.TransactionAsync(Language.Save, async db =>
        {
            post.ReplyQty = (post.ReplyQty ?? 0) + 1;
            post.CalculateRank();
            await db.SaveAsync(post);
            model.ReplyTime = DateTime.Now;
            await db.SaveAsync(model);
            await db.AddUserCountAsync(BizType.Reply, CurrentUser.Id);
        }, model);
    }

    public Task<CmUser> GetUserAsync(string id)
    {
        return Database.QueryByIdAsync<CmUser>(id);
    }

    public async Task<Result> SaveUserAsync(UserFormInfo info)
    {
        var model = await Database.QueryByIdAsync<CmUser>(Context.CurrentUser.Id);
        if (model == null)
            return Result.Error("用户不存在！");

        model.UserName = info.UserName;
        model.NickName = info.NickName;
        model.Sex = info.Sex;
        model.Metier = info.Metier;
        if (await SiteRepository.ExistsUserNameAsync(Database, model))
            return Result.Error("用户名已存在！");

        return await Database.TransactionAsync(Language.Save, async db =>
        {
            await db.SaveAsync(model);
        }, model);
    }

    public async Task<Result> SaveUserPwdAsync(PwdFormInfo info)
    {
        var model = await Database.QueryByIdAsync<CmUser>(Context.CurrentUser.Id);
        if (model == null)
            return Result.Error("用户不存在！");

        if (info.NewPwd != info.NewPwd1)
            return Result.Error("两次密码不一致！");

        return await Database.TransactionAsync(Language.Save, async db =>
        {
            model.Password = Utils.ToMd5(info.NewPwd);
            await db.SaveAsync(model);
        }, model);
    }

    private static async Task<CmPost> GetPostByDocumentAsync(Database db, ContentType type, string code)
    {
        var categories = await db.QueryListAsync<CmCategory>(d => d.Type == type.ToString());
        if (string.IsNullOrWhiteSpace(code))
        {
            var first = categories?.Where(c => c.ParentId == "0").OrderBy(c => c.Sort).FirstOrDefault();
            if (type == ContentType.UpdateLog)
            {
                first = categories?.Where(c => c.ParentId == "0").OrderBy(c => c.Sort).LastOrDefault();
                code = first?.Code;
            }
            else
            {
                var firstPage = categories?.Where(c => c.ParentId == first?.Id).OrderBy(c => c.Sort).FirstOrDefault();
                code = firstPage?.Code;
            }
        }
        var category = categories.FirstOrDefault(c => c.Code.Equals(code, StringComparison.OrdinalIgnoreCase));
        if (category == null)
            return null;

        return await db.QueryByIdAsync<CmPost>(category.Id);
    }

    private static async Task<List<ReplyListInfo>> GetReplyListsAsync(Database db, string postId)
    {
        var replies = await db.QueryListAsync<CmReply>(d => d.BizId == postId);
        if (replies == null || replies.Count == 0)
            return null;

        var infos = new List<ReplyListInfo>();
        replies = [.. replies.OrderByDescending(c => c.ReplyTime)];
        foreach (var item in replies)
        {
            var user = await db.QueryByIdAsync<CmUser>(item.UserId);
            var info = new ReplyListInfo
            {
                Id = item.Id,
                BizId = item.BizId,
                Author = new UserInfo
                {
                    Name = user?.NickName,
                    AvatarUrl = user?.AvatarUrl
                },
                Content = item.Content,
                ReplyTime = item.ReplyTime
            };
            infos.Add(info);
        }
        return infos;
    }
}