﻿namespace KnownCMS.Helpers;

class AIHelper
{
    private static readonly Dictionary<string, string[]> Tags = new()
    {
        ["前端"] = ["前端", "界面", "Blazor", "UI"],
        ["后端"] = ["后端", "服务", "数据依赖", "Service", "Repository"],
        ["数据库"] = ["数据库", "Database"],
        ["需求"] = ["需求"],
        ["设计"] = ["设计"],
        ["建议"] = ["建议"],
        ["BUG"] = ["BUG", "Exception", "异常"]
    };

    // 根据内容自动识别类别
    public static string GetPostCategory(List<CmCategory> categories, PostFormInfo info)
    {
        return string.Empty;
    }

    // 根据内容自动识别标签
    public static string GetPostTags(PostFormInfo info)
    {
        var tags = new List<string>();
        foreach (var tag in Tags)
        {
            foreach (var key in tag.Value)
            {
                if (info.Title.Contains(key) || info.Content.Contains(key))
                {
                    tags.Add(tag.Key);
                    continue;
                }
            }
        }
        if (tags.Count == 0)
            tags.Add("其他");
        return string.Join(",", tags);
    }
}