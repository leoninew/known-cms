﻿namespace KnownCMS.Web.Pages.Interact;

[Route("/ims/categories")]
public class ImCategoryList : CategoryList
{
    protected override ContentType Type => ContentType.Interact;
}