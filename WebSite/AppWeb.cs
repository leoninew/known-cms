﻿namespace WebSite;

static class AppWeb
{
    internal const string AuthType = "Known_Cookie";

    public static void AddApp(this WebApplicationBuilder builder)
    {
        foreach (var item in Language.Items)
        {
            item.Visible = item.Id != "en-US" && item.Id != "vi-VN";
        }

        //ModuleHelper.InitAppModules();
        //Stopwatcher.Enabled = true;
        builder.Services.AddAuthentication().AddCookie(AuthType);
        builder.Services.AddCMSLite();
        builder.Services.AddKnownCore(info =>
        {
            info.WebRoot = builder.Environment.WebRootPath;
            info.ContentRoot = builder.Environment.ContentRootPath;
            info.Connections = [new Known.ConnectionInfo
            {
                Name = "Default",
                DatabaseType = DatabaseType.SQLite,
                ProviderType = typeof(Microsoft.Data.Sqlite.SqliteFactory),
                ConnectionString = builder.Configuration.GetSection("ConnString").Get<string>()
            }];
        });
        builder.Services.AddKnownWeb();
        builder.Services.AddKnownWeixin(builder.Configuration);
    }

    public static void UseApp(this WebApplication app)
    {
        app.UseKnown();
        app.UseKnownWeixin();
    }
}